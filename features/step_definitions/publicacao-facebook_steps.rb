include RSpec::Matchers

Dado('que eu acesse o Facebook') do
  @GooglePage = GooglePage.new
  @GooglePage.acessa_facebook_por_consulta_no_google
  @LoginPage = LoginPage.new
  @LoginPage.faz_login(CUSTOM['user_name_prod2'], CUSTOM['password_prod'])
  end
  
  Quando('eu faço uma publicação') do
    @FacebookPage = FacebookPage.new
    @FacebookPage.insere_publicacao
  end
  
  Então('o sistema deve mostrar a publicação na minha timeline') do
    @FacebookPage = FacebookPage.new
    @FacebookPage.confirma_publicacao
  end