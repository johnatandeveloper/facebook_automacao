include RSpec::Matchers

Dado('que eu acesse o Facebook a partir de uma pesquisa no Google') do
  @GooglePage = GooglePage.new
  @GooglePage.acessa_facebook_por_consulta_no_google
end

Quando('eu faço o login') do
  @LoginPage = LoginPage.new
  @LoginPage.faz_login(CUSTOM['user_name_prod2'], CUSTOM['password_prod'])
end

Então('devo confirmar a cor de fundo do cabeçalho') do
  @FacebookPage = FacebookPage.new
  @FacebookPage.confirma_cor_fundo_cabecalho 
end

Então('confirmar usuário logado') do
  @FacebookPage = FacebookPage.new
  @FacebookPage.confirma_usuario_logado
end