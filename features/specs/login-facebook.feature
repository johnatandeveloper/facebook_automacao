#language:pt
@login
Funcionalidade: Login - Facebook

  Para acessar o Facebook a partir do Google
  Como um usuário do Facebook
  Quero validar as informações de acesso

Cenário: Acessar Facebook a partir do Google, fazer login e validar dados 

  Dado que eu acesse o Facebook a partir de uma pesquisa no Google
  Quando eu faço o login
  Então devo confirmar a cor de fundo do cabeçalho
  E confirmar usuário logado