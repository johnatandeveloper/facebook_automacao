#language:pt
@publicacao
Funcionalidade: Publicação no Facebook

  Para publicar no Facebook
  Como um usuário que acabou de postar
  Quero validar as informações da postagem

Cenário: Confirmar publicação realizada 

  Dado que eu acesse o Facebook
  Quando eu faço uma publicação
  Então o sistema deve mostrar a publicação na minha timeline