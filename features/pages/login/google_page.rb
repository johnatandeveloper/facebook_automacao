class GooglePage < SitePrism::Page
    
    include Capybara::DSL

    #Pesquisa Google
    element :input_pesquisa, "input[name='q']"
    element :div_a_link, "div[class='g'] div[class='tF2Cxc'] a[href*='pt-br.facebook'] h3[class='LC20lb DKV0Md']", :text => "Facebook – entre ou cadastre-se"

    def acessa_facebook_por_consulta_no_google
        input_pesquisa.send_keys("facebook entre").native.send_keys(:enter)
        div_a_link.click    
    end

end
