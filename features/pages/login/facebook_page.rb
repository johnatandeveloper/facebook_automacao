class FacebookPage < SitePrism::Page

    include Capybara::DSL
        
    #Tela Facebook
    element :body_cor, "body"
    element :div_span_nome, "div[class='sjgh65i0'] div[class='m9osqain a5q79mjw jm1wdb64 k4urcfbm'] span"

    #Publicação
    element :span_div_perfil_nome, ".qzhwtbm6.knvmm38d span[dir=auto]",  :text => "Johnatan Carvas"
    element :div_span_iniciar_publicacao,  "div[data-pagelet='ProfileComposer'] .a8c37x1j.ni8dbmo4.stjgntxs.l9j0dhe7", :text => "No que você está pensando?"
    element :div_span_add_texto_publicacao, "div[role='presentation'] div[class=_5rp7] div[class=_5rpb] span ", visible: false
    element :div_span_button_publicar, "div[class='ihqw7lf3 discj3wi l9j0dhe7'] div[aria-label='Publicar'] span[dir='auto'] ", :text => "Publicar"
    element :div_feed_de_publicacao, "div[data-pagelet='ProfileTimeline']" 
    element :div_panel_add_publicacao, "div[role='presentation'] span", :text => "Criar publicação"
    
    def confirma_cor_fundo_cabecalho 
        cor = body_cor.native.css_value('background-color')
        expect(cor).to eq('rgba(240, 242, 245, 1)')
    end

    def confirma_usuario_logado
        expect(div_span_bem_vindo.text()).to include "No que você está pensando, JohncarvasTeste?"
    end

    def insere_publicacao
        @texto_publicacao = Faker::Music.album
        span_div_perfil_nome.click
        div_span_iniciar_publicacao.click
        div_span_add_texto_publicacao.send_keys("#{@texto_publicacao}")
        div_span_button_publicar.click
    end

    def confirma_publicacao
        wait_until_div_span_button_publicar_invisible
        expect(div_feed_de_publicacao.text()).to include "#{@texto_publicacao}"
    end
end
