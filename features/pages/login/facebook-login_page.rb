class LoginPage < SitePrism::Page
  
  include Capybara::DSL
 
  #Login Facebook
  element :input_user, "#email"
  element :input_pass, "#pass"
  element :button_login, "button[name='login']"
  element :div_notifica_login, "div[class='_9ay7']"

  def faz_login(login, senha)
    input_user.set(login)
    input_pass.set(senha)
    button_login.click
  end

end